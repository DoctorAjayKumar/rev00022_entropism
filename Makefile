all: setup build clean

project = rev00022_entropism

setup:
	mkdir -p .build
	cp -rv \
		include \
	  *.tex \
	  .build

build:
	cd .build && \
	  latexmk -pdf frame.tex && \
	  cp frame.pdf ..
	ln -sf frame.pdf $(project).pdf

clean:
	rm -rf .build
