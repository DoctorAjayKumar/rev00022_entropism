% File: complex1_lex.xrl

Definitions.

WHITESPACE = [\s\t\n\r]

Rules.

everybody       : {token, {noun, TokenLine, TokenChars}}.
founder         : {token, {noun, TokenLine, TokenChars}}.
he              : {token, {noun, TokenLine, TokenChars}}.
i               : {token, {noun, TokenLine, TokenChars}}.
is              : {token, {verb, TokenLine, TokenChars}}.
know            : {token, {verb, TokenLine, TokenChars}}.
love            : {token, {verb, TokenLine, TokenChars}}.
poops           : {token, {verb, TokenLine, TokenChars}}.
the             : {token, {determiner, TokenLine, TokenChars}}.

{WHITESPACE}+   : skip_token.

Erlang code.
